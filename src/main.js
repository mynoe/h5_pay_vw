import Vue from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import store from "./store";
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import Vum from 'vum'
import api from './http/api/index'
import VueCropper from 'vue-cropper'
Vue.use(VueCropper)
// import style
Vue.prototype.$api = api
Vue.config.productionTip = false;
Vue.use(ElementUI);
Vue.use(Vum)
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
