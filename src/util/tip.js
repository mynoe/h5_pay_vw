import { Message } from 'view-design';

function success(msg,cb){
  Message.success(msg)
  if(cb && typeof cb ==='function'){
    cb()
  }
}

function error(msg,cb){
  Message.error(msg)
  if(cb && typeof cb ==='function'){
    cb()
  }
}

export default function tip(code,msg,cb){
  if(code === 200){
    success(msg,cb)
  }
  error(msg,cb)
}

export default tip

