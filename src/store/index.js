import Vue from "vue";
import Vuex from "vuex";
import api from '../http/api/index'

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    token: {},
  },
  mutations: {
    setToken(state, obj) {
      let Token = JSON.parse(localStorage.getItem('Token'))
      localStorage.token = JSON.stringify(obj)
      if(Token){
        state.token = { ...Token }
      }
      state.token = { ...obj }
    },
  },
  actions: {
  },
  modules: {}
});
