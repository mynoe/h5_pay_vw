import Vue from 'vue'
import VueRouter from 'vue-router'
import Invite from '../views/Invite.vue'
import InviteCard from '../views/InviteCard'
import Member from '../views/Member'
import Login from '../views/Login'
import Gender from '../views/Gender'
import Regist from '../views/Regist'
import ValidateCode from '../views/ValidateCode.vue'
import WechatPay from '../views/WechatPay.vue'
import Goddess from '../views/Goddess.vue'
// 
import Home from '../views/Home'
// 游戏界面
import GameIndex from '../views/GameIndex.vue'
import GameContent from '../views/GameContent'

Vue.use(VueRouter)

const routes = [
  {
    path: '',
    // redirect: '/member'
    redirect:'/goddess'
  },
  {
    path: '/home',
    name: 'Home',
    component: Home
  },
  {
    path: '/invite',
    name: 'Invite',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ '../views/Invite.vue'),
  },
  {
    path: '/invite',
    name: 'Invite',
    component: Invite
  },
  {
    path: '/member',
    name: 'Member',
    component: Member
  },
  {
    path: '/wechatpay',
    name: 'WechatPay',
    component: WechatPay
  },
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  {
    path:'/inviteCard',
    name:InviteCard,
    component:InviteCard
  },
  {
    path:'/gender',
    name:Gender,
    component:Gender
  },
  {
    path:'/regist',
    name:'Regist',
    component:Regist
  },
  {
    path:'/validateCode',
    name:'ValidateCode',
    component:ValidateCode
  },
  {
    path:'/goddess',
    name:'Goddess',
    component:Goddess
  },
  {
    path:'/game',
    name:'Game',
    component:GameIndex
  },
  {
    path:'/gameContent',
    name:'GameContent',
    component:GameContent
  }
]

const router = new VueRouter({
  mode: 'hash',
  // mode:'history',
  base: process.env.BASE_URL,
  routes,
})

export default router

