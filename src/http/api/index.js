import axios from 'axios'
import '../config'
import qs from 'querystring'
import user from './user'
let api = {
    ...user,
}
export default api
