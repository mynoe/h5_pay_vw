import axios from 'axios'
import qs from 'querystring'
let user = {
  /**
   * name
   * password
   * */

  login(params) {
    // return axios.post('/login', params)
    return axios.get(`/login?${qs.stringify(params)}`)
  },
  logout() {
    return axios.post('index/logout')
  },
  /**
   * 获取验证码
   * @param {*} params 
   */

  getCode(params) {
    // return axios.get(`/h5/sso/sendVerifyCode?${qs.stringify(params)}` )
    return axios.post(`/hapi/sso/sendVerifyCode`,params )
  },
  // 注册
  regist(params){
    return axios.post(`/hapi/sso/register`,params)
  },
  // 交友节目
  getFriendTags(params){
    return axios.post(`/hapi/common/tags`,params)
  },
  // 城市列表
  getCityList(params){
    return axios.post(`/hapi/common/openCityList`,params)
  },
  // 保存用户
  // {
  //   headers:{'Content-Type':'application/x-www-form-urlencoded'}
  // }
  saveUserInfo(params,conf){
    return axios.post(`/hapi/user/save`,qs.stringify(params),conf)
  },
  // 检测昵称可用
  checkNickNane(params){
    return axios.post(`/hapi/user/checkUserName`,params)
  },
  // 邀请人信息
  getInviterInfo(params,conf){
    console.log(
      qs.stringify(params)
    )
    return axios.post(`/hapi/user/inviteUserInfo`,qs.stringify(params),
    {
      headers:{'Content-Type':'application/x-www-form-urlencoded'}
    })
  },
  // 会员信息
  getMemberInfo(params){
    return axios.post(`/hapi/level/conf`,params)
  },
  // 支付下单
  payOrder(params){
    return axios.post(`/hapi/aliPay/prePay`,params)
  },
  // 微信支付 h5下单
  wcPayOrder(params){
    return axios.post(`/hapi/wechatPay/wapPay`,params)
  },
  // 获取微信openId
  getWxOpenId(params){
    return axios.post(`/hapi/wechatPay/getOpenId`,params)
  },
  // 微信内支付
  wcInnerPayOrder(params){
    return axios.post(`/hapi/wechatPay/prePay`,params)
  },
  //用户初始化
  userInit(params){
    return axios.post(`/hapi/user/init`,params)
  },
  // 阿里token
  getAliToken(params){
    return axios.post(`/hapi/sts/token`,params)
  },
  // 获取下载地址
  getDownLoadUrl(params){
    return axios.post(`/hapi/sso/getAppDownloadUrl`,params)
  }
}
export default user
