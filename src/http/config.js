import axios from 'axios'
import store from '@/store/'

// axios.defaults.headers.post['Content-Type'] = 'application/json;charset=UTF-8'
axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded'
axios.defaults.baseURL = process.env.VUE_APP_API_BASE_URL
console.log('--',process.env)
console.log(process.env.VUE_APP_API_BASE_URL)
axios.defaults.withCredentials = true

// 添加请求拦截器
axios.interceptors.request.use(
  config => {
    if (store.state.token) {
      console.log('store.state.token',store.state.token)
      let Token 
      if(localStorage.Token){
       Token = JSON.parse(localStorage.Token)
      }
      if(Token){
        config.headers.Authorization = Token.token_type +' '+ Token.access_token
      }
      // 如果token存在
      // config.headers.Authorization = store.state.token.token_type +' '+ store.state.token.access_token
    }
    return config
  },
  error => {
    // 对请求错误做些什么
    return Promise.reject(error)
  }
)

// 添加响应拦截器
axios.interceptors.response.use(
  response => {
    if (response.data) {
      if (response.data.code === 2) {
        Message.error({
          content: response.data.msg,
          duration: 3,
          closable: true
        })
        return
      }
    }
    return response
  },
  error => {
    if (error.response) {
      switch (error.response.status) {
        case 401:
          // 401 操作
          break
        case 505:
          Message.error({
            content: '505,服务端错误!',
            duration: 3,
            closable: true
          })
      }
    }
    return Promise.reject(error)
  }
)
