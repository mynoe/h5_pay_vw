var path = require('path')
function resolve(dir) {
  return path.join(__dirname, './', dir)
}
module.exports = {
  //应用是被部署在一个域名的根路径上，例如 https://www.my-app.com/。
  //如果应用被部署在一个子路径上，你就需要用这个选项指定这个子路径。
  //例如，如果你的应用被部署在
  //https://www.my-app.com/my-app/，则设置 publicPath 为 /my-app/
  // publicPath: process.env.NODE_ENV === 'production'? '/': '/h5',
  publicPath:'',
  outputDir: 'dist',
  // 放置生成的静态资源 (js、css、img、fonts) 的 (相对于 outputDir 的) 目录。
  assetsDir: 'asstes',
  // 以下是pwa配置
  pwa         : {
    iconPaths: {
      favicon32     : 'img/icons/favicon.png',
      favicon16     : 'img/icons/favicon.png',
      appleTouchIcon: 'img/icons/favicon.png',
      maskIcon      : 'img/icons/favicon.png',
      msTileImage   : 'img/icons/favicon.png'
    }
  },
  css: {
    // loaderOptions: {
    //   // 给 sass-loader 传递选项
    //   sass: {
    //     // @/ 是 src/ 的别名
    //     // 所以这里假设你有 `src/variables.sass` 这个文件
    //     // 注意：在 sass-loader v8 中，这个选项名是 "prependData"
    //     // additionalData: `@import "~@/variables.sass"`
    //   },
    // }
  },
  configureWebpack: {
    module: {
      rules: [
        {
          // test: /\.vue$/,
          // use: [
          //   {
          //     loader: 'iview-loader',
          //     options: {
          //       prefix: true
          //     }
          //   }
          // ]
        },
        // {
        //   test: /\.s[ac]ss$/,
        //   // loaders: ['style', 'css', 'sass'],
        //   use: [
        //     // Creates `style` nodes from JS strings
        //     'style-loader',
        //     // Translates CSS into CommonJS
        //     'css-loader',
        //     // Compiles Sass to CSS
        //     'sass-loader',
        //   ],
        // },
      ],
    },
  },
  devServer: {
    port: 8089,
    proxy: {
      '': {
        // target: 'http://39.101.174.83:8899',
        target: 'http://test.belove-vip.com/',
        changeOrigin: true,
        pathRewrite: {
          '^/api': '',
        },
      },
    },
  },
}
