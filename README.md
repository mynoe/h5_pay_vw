# H5支付系统

# 本地开发
1. 进入项目根目录 执行 `npm install` 安装项目所需依赖
2. 执行 `npm run start` 或  `yarn start`进行本地开发调试

# 项目发布 && 配置
1. 在项目根目录执行 `npm run build` 进行文件打包
2. 打包后的静态文件会在 build文件夹下
3. 将build 文件夹下的静态文件 上传到服务器指定文件夹即可

```
  //应用是被部署在一个域名的根路径上，例如 https://www.my-app.com/。
  //如果应用被部署在一个子路径上，你就需要用这个选项指定这个子路径。
  //例如，如果你的应用被部署在
  //https://www.my-app.com/my-app/，则设置 publicPath 为 /my-app/
  publicPath: '/h5_pay',
  outputDir: 'dist',
  // 放置生成的静态资源 (js、css、img、fonts) 的 (相对于 outputDir 的) 目录。
  assetsDir: 'asstes',
```

# 发布可能会遇到的问题
1. 图片不显示  接口返回的是相对路径，部署到不同域名下可能会出现访问不到的情况

# 流程中可能遇到问题
1. 前往公众号 缺跳转路径 跳转方法
2. 下载APP 链接 暂无
3. 邀请人链接 参数携带方法 待定
4. 支付 quitUrl:'',returnUrl:'' 待定

# 微信支付调试问题
1. 确认网页H5支付回调地址 ok 已处理
2. 微信JSapi支付 公众号配置的域名 ok 已处理
3. 需要测试环境上调试支付 ， 本地无法测试支付功能 ok 已处理

# 测试地址
1. http://test-m.belove-vip.com/goddess?inviteCode=yT7jPg
# 游戏地址
1. http://test-m.belove-vip.com/game
        



